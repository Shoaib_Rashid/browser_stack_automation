import requests
import json
import pandas as pd
import numpy as np
import time
from google.cloud import bigquery
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from get_expiry_os import expiry_os
import logging
import os



class find_new_os():

    def __init__(self, ):
        self.stack_df = None
        self.present_os = None
        self.old_df = None
        self.not_os_df = None
        self.new_df = None
        self.latest_df = None

    def get_stack_data(self, ):
        try:
            r = requests.get(url='https://api.browserstack.com/5/browsers?flat=true&all=true',
                             auth=('neeraj110', '7hoQsdqshwSWsznfpp5m'))
            response = (r.text.encode("ascii"))
            response = json.loads(response)
            df_new = pd.DataFrame(response)
            # droping rows which have null value in browser version reason when you want to test in browser stack you have specify browser version and browser
            self.stack_df = df_new.dropna(axis=0, subset=['browser_version'])
            # available os in browser stack
            self.present_os = self.stack_df['browser'] + ' ' + self.stack_df['browser_version']

        except Exception as e:
            logging.error('credential data is not matched', exc_info = True)

    def get_query(self, ):
        """ query to get past data from bigquery """
        try:
            query = """SELECT *FROM [shieldsquare-team-research.browser_stack.11593_190704]
                where et  = 81"""
            return query
        except Exception as e:
            logging.error('query incorrect {}'.format(e), exc_info= True)

    def run_query(self, ):
        """getting previous store data in bigquery """
        try:
            client = bigquery.Client()
            query_config = bigquery.QueryJobConfig(use_legacy_sql=True)
            self.old_df = client.query(self.get_query(), job_config=query_config,
                                       project="ss-production-storage").to_dataframe()
        except Exception as e:
            logging.error('unavailable to get result, error: {}'.format(e), exc_info =  True)

    def not_present_os(self, ):
        """ getting list of new browser """
        try:
            find_os = []
            # getting browser and version
            for i in self.old_df['brwsrVer'].apply(lambda x: x.split('.', 1)):
                find_os.append(i[0] + '.' + i[1].split('.')[0])
            # combining present os and expiry_os
            find_os = find_os + expiry_os
            find_os_series = pd.Series(find_os)
            find_os_series = find_os_series.apply(lambda x: x.upper())
            # eliminating old os and expiry os list
            self.not_os_df = self.stack_df[self.present_os.apply(lambda x: x.upper() not in list(find_os_series))]
            print(self.not_os_df)
        except Exception as e:
            logging.error('parsing error : {}'.format(e), exc_info = True)

    def get_data_for_new_os(self, data):

        """ calling browser stack with new info """
        try:
            req = requests.post(url='https://api.browserstack.com/5/worker', auth=('neeraj110', '7hoQsdqshwSWsznfpp5m'),
                                data=data)
            response_final = (req.text.encode("ascii"))
            response_final = json.loads(response_final)
            req_service = requests.get(url="https://api.browserstack.com/5/worker/{}".format(response_final["id"]),
                                       auth=('neeraj110', '7hoQsdqshwSWsznfpp5m'))
            response_final_service = req_service.text.encode("ascii")
            response_final_service = json.loads(response_final_service)
            time.sleep(50)
            output = requests.delete("https://api.browserstack.com/5/worker/{}".format(response_final["id"]),
                                     auth=('neeraj110', '7hoQsdqshwSWsznfpp5m'))

        except Exception as e:
            logging.error('request fail,error: '.format(e),exc_info = True)

    def loop_over_new_os(self, ):
        """ running loop for new browser present in browser stack api """
        try:
            for i in self.not_os_df.index:
                data = {
                    "device": None,
                    "real_mobile": None,
                    "os_version": self.stack_df.iloc[i, 4],
                    "os": self.stack_df.iloc[i, 3],
                    "browser_version": self.stack_df.iloc[i, 1],
                    "browser": self.stack_df.iloc[i, 0],
                    "url": "http://ssjswebsite.tk/event/raw.html?" + str(9000 + i),
                    "timeout": 40
                }
                self.get_data_for_new_os(data)
        except Exception as e:
            logging.error('incorrect index, error: {}'.format(e), exc_info = True)


    def get_latest_data(self, ):
        """ get browser stack data from bigquery """
        try:
            now = datetime.datetime.now()
            # creating query to get the info of js data from  biquery of present day
            query = """SELECT
                os, brwsrVer, _zpsbd4
                FROM
                  TABLE_QUERY([ss-production-storage:Citadel_Fern],
                    'table_id like "t_11593_{0}{1}{2}_%"'),"""
            query = query.format(str(now.year)[2:], str(now.month).zfill(2), str(now.day).zfill(2))

            client = bigquery.Client()
            query_config = bigquery.QueryJobConfig(use_legacy_sql=True)
            self.new_df = client.query(query, job_config=query_config, project="ss-production-storage").to_dataframe()
            print(self.new_df)
        except Exception as e:
            logging.error('Table is not found {}'.format(e), exc_info = True)

    def upload_data(self, ):
        """ push new browser info into bigquery table"""
        try:
            client = bigquery.Client(project='shieldsquare-team-research')
            dataset_ref = client.dataset('browser_stack')
            table_ref = dataset_ref.table('11593_190704')  # table name where to push the data
            # push the dataframe to the table
            client.load_table_from_dataframe(self.new_df, table_ref, project=project_id).result()
        except Exception as e:
            logging.error('Table is not found in shiledsquare project, error:'.format(e), exc_info = true)


if __name__ == '__main__':
    try:
        if not os.path.exists('stack.log'):
            f = open('stack.log', mode = 'w+')
            f.close()
        else:
            logging.basicConfig(filename = 'stack.log', filemode = 'a', format = '%(asctime)s - %(message)s', level = logging.DEBUG)

        obj = find_new_os()
        obj.get_stack_data()
        obj.run_query()
        obj.not_present_os()
        obj.loop_over_new_os()
        obj.get_latest_data()
        obj.upload_data()
    except Exception as e:
        logging.error('error:'.format(e),exc_info = True)
